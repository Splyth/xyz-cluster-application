# xyz-cluster-application

A test application which spins up a small python http server and returns a json value.
When checked in a pipeline will push it to AWS ECR

## Prerequisites
- AWS Account with ECR Push Access
- python
- docker

## Usage

### Run Server
1. run: python3 server.py

### Run Server in Docker Container
1. `docker build -t xyz-app .`
2. `docker run -d -p 8000:80 --name xyz-app xyz-app`

### Query Server
1. `localhost:8000`


## New Project Setup

1. Fork the project 
2. Open your Gitlab Repo's Settings -> CI/CD
3. Expand Variables
4. Add Variable: AWS_ACESS_KEY Value: Your AWS Account's AWS ACCESS KEY
5. Add Variable: AWS_SECRET_ACESS_KEY Value: Your AWS Account's AWS SECRET ACCESS KEY
6. Add Variable: TF_VAR_agent_token Value: Your Gitlab Agent Token
7. Add Variable: TF_VAR_kas_address Value: Your Gitlab Agent Token Key Access Address