FROM python

WORKDIR /server
COPY . .
ENTRYPOINT [ "python3", "server.py" ]
EXPOSE 80